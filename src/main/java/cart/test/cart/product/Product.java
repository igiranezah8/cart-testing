package cart.test.cart.product;


import cart.test.cart.pricing.DiscountStrategy;
import cart.test.cart.pricing.NoDiscountStrategy;

public class Product {
    private String name;
    private int unitPrice;
    private DiscountStrategy discountStrategy;

    public Product(String name, int unitPrice, NoDiscountStrategy discountStrategy) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.discountStrategy = discountStrategy;
    }

    public int getPriceForQuantity(int quantity) {
        int priceWithoutDiscount = unitPrice * quantity;
        int discount = getDiscount(quantity);
        return priceWithoutDiscount - discount;
    }

    private int getDiscount(int quantity) {
        return discountStrategy.getDiscount(unitPrice, quantity);
    }
}
