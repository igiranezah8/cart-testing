package cart.test.cart.cart;

import cart.test.cart.product.Product;

import java.util.HashMap;
import java.util.Map;

public class ShoppingCart {
    Map<Product, Integer> quantities = new HashMap<>();

    public void addProduct(Product product) {
        int currentQuantity = getCurrentQuantity(product);
        quantities.put(product, currentQuantity + 1);
    }

    public void removeProduct(Product product) {
        int currentQuantity = getCurrentQuantity(product);
        quantities.put(product, currentQuantity - 1);
    }

    private int getCurrentQuantity(Product product) {
        return quantities.get(product) == null ? 0 : quantities.get(product);
    }

    public int getTotalPrice() {
        int totalPrice = 0;
        for (Product product : quantities.keySet()) {
            totalPrice += product.getPriceForQuantity(quantities.get(product));
        }
        return totalPrice;
    }
}
