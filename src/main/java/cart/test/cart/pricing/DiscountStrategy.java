package cart.test.cart.pricing;

public interface DiscountStrategy {
    int getDiscount(int unitPrice, int quantity);

    String getName();
}
