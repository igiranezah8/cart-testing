package cart.test.cart.cart;

import cart.test.cart.pricing.DiscountStrategy;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import cart.test.cart.pricing.NoDiscountStrategy;
import cart.test.cart.product.Product;
@RunWith(MockitoJUnitRunner.class)
public class ShoppingCartTest {
    private ShoppingCart cart = new ShoppingCart();

    @Mock
    private DiscountStrategy discountStrategy;

    private Product product1 = new Product("TestName1", 1000, new NoDiscountStrategy());
    private Product product2 = new Product("TestName2", 5000, new NoDiscountStrategy());

    @Test
    public void addNewProduct_withElements() throws Exception {
        cart.addProduct(product1);
        assertEquals(1, cart.quantities.get(product1).intValue());
    }

    @Test
    public void removeProduct() throws Exception {
        cart.removeProduct(product2);
        assertEquals(-1, cart.quantities.get(product2).intValue());
    }

    @Test
    public void increaseQuantity_whenProductAdded() throws Exception{
    cart.addProduct(product1);
    cart.addProduct(product1);
    assertEquals(2, cart.quantities.get(product1).intValue());
    }

    @Test
    public void reduceQuantity_whenProductRemoved() throws Exception{
        cart.removeProduct(product1);
        cart.removeProduct(product1);
        assertEquals(-2, cart.quantities.get(product1).intValue());
    }

    @Test
    public void totalPriceIsZero_whenCartEmpty() throws Exception {
        int totalPrice = cart.getTotalPrice();
        assertEquals(0, totalPrice);
    }

    @Test
    public void calculateTotalPrice_withElements() throws Exception {
        cart.addProduct(product1);
        cart.addProduct(product1);
        cart.addProduct(product2);
        cart.addProduct(product2);
        cart.addProduct(product2);
        cart.removeProduct(product2);
        cart.removeProduct(product1);
        int totalPrice = cart.getTotalPrice();
        assertEquals(11000, totalPrice);
    }

}
